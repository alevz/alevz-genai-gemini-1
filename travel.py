import vertexai
from vertexai.preview import generative_models
from vertexai.preview.generative_models import (
    GenerativeModel,
    Part,
    Image,
)
from vertexai.preview.vision_models import ImageGenerationModel
import requests
import streamlit as st
import os
import string
import random

#init
vertexai.init(location="asia-southeast1")

#forEasierCustProfile

col1, col2, col3 = st.columns(3)

with col1:
    gender = st.selectbox(
        "Sample Gender",
        ('Male', 'Female'))

with col2:
    age = st.selectbox(
        "Sample Age",
        ('10-20', '20-30', '30-40', '40-50', '50-60'))

with col3:
    ethnicity = st.selectbox(
        "Sample Ethnicity",
        ('Asian', 'Black', 'Hispanic', 'White'))

#upload file
uploaded_file = st.file_uploader("Choose a file")

#progress bar
progress_text = "Start"
my_bar = st.progress(0, text=progress_text)

#getting weather from openweathermap
def get_current_weather(location, unit="metric"):
    api_key = os.environ["API_KEY"]
    url = f"https://api.openweathermap.org/data/2.5/weather?q={location}&units={unit}&appid={api_key}"
    response = requests.get(url)
    
    return response.json()

#function declaration
get_current_weather_func = generative_models.FunctionDeclaration(
  name="get_current_weather",
  description="Get the current weather in a given location",
  parameters={
      "type": "object",
      "properties": {
          "location": {
              "type": "string",
              "description": "The city and state, e.g. San Francisco, CA"
          },
          "unit": {
              "type": "string",
              "enum": [
                  "celsius",
                  "fahrenheit",
              ]
          }
      },
      "required": [
          "location"
      ]
  },
)

#adding tools 
weather_tool = generative_models.Tool(
  function_declarations=[get_current_weather_func]
)


#activity
if uploaded_file is not None:

    st.image(uploaded_file)
    bytes_data = uploaded_file.getvalue()
    uploaded_file_read = Image.from_bytes(bytes_data)

    my_bar.progress(10, text="processing image")

    #getting location from image using gemini-pro-vision
    model_vision = GenerativeModel("gemini-1.0-pro-vision")
    response_vision = model_vision.generate_content([
        uploaded_file_read, "give me only the name of the city where the picture is taken"
    ])

    #print for debug
    print("gemini-1.0-pro-vision\n",response_vision)
    st.toast(response_vision._raw_response.usage_metadata)

    #print for user
    st.write("Location:",response_vision.text)

    my_bar.progress(30, text="getting location from image")
    location1 = response_vision.text
    
    #getting result from prompt + function calling
    prompt = f"Can please tell me what is current weather real temperature, description and feels like in {location1} today"

    model = GenerativeModel("gemini-1.0-pro", 
                            tools=[weather_tool])

    chat = model.start_chat()

    #first init prompt gemini-pro
    model_response = chat.send_message(prompt)

    #print for debug
    print("gemini-pro: first call\n", model_response)
    my_bar.progress(50, text="prompt to gemini")

    api_response = get_current_weather(model_response.candidates[0].content.parts[0].function_call.args['location'],"metric")
    print("api_response",api_response)
    print("api_response",api_response['main']['temp'])
    temperature=api_response['main']['temp']

    my_bar.progress(70, text="get weather")

    #adding function_call
    model_response = chat.send_message(
        Part.from_function_response(
            name="get_current_weather",
            response={
                "content": api_response,
            }
        ),
    )

    st.toast(model_response._raw_response.usage_metadata)
    st.write(model_response.text)
    
    my_bar.progress(85, text="add weather information")

    prompt = f"summary about the {location1} and give me recommendation things to do and how to get there from Jakarta"
    model_response = chat.send_message(prompt)

    #for debug
    print("gemini-pro: adding function call", model_response)
    st.toast(model_response._raw_response.usage_metadata)
    st.write(model_response.text)

    my_bar.progress(85, text="add a personal touch")

    #testing imagenv2
    custprofile = {
        "gender": "male",
        "age": "30-40",
        "ethnicity": "asian"
    }

    col1,col2 = st.columns(2)
    model_imagen = ImageGenerationModel.from_pretrained("imagegeneration@005")
    images = model_imagen.generate_images(
        prompt=f"portrait of traveling to {location1} {gender} age {age} years old {ethnicity} {temperature} degrees celcius happy traveling, with word \"IMAGEN\" on clothes, taken from behind",
        # Optional:
        number_of_images=2
        #seed=1
    )

    print("imagen response",images)

    N=7
    res = ''.join(random.choices(string.ascii_uppercase +
                            string.digits, k=N))
    res2 = ''.join(random.choices(string.ascii_uppercase +
                            string.digits, k=N))
    
    try:
        
        images[0].save(location=f"/temp/gen-img-{str(res)}.png", include_generation_parameters=True)
        images[1].save(location=f"/temp/gen-img-{str(res2)}.png", include_generation_parameters=True)
        col1.image(f'/temp/gen-img-{str(res)}.png', caption=f'Travel to {location1}')
        col2.image(f'/temp/gen-img-{str(res2)}.png', caption=f'Travel to {location1}')
    except(RuntimeError,TypeError,NameError):
        col1.image('./general.png', caption=f'Travel to {location1}')
        col2.image('./general.png', caption=f'Travel to {location1}')
        print("some error")

    my_bar.progress(100, text="Complete")