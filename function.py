from vertexai.preview import generative_models
from vertexai.preview.generative_models import (
    Content,
    FunctionDeclaration,
    GenerativeModel,
    Part,
    Tool,
)
import requests


def get_current_weather(location, unit="metric"):
    api_key = ""
    url = f"https://api.openweathermap.org/data/2.5/weather?q={location}&units={unit}&appid={api_key}"
    response = requests.get(url)
    
    return response.json()
    

get_current_weather_func = generative_models.FunctionDeclaration(
  name="get_current_weather",
  description="Get the current weather in a given location",
  parameters={
      "type": "object",
      "properties": {
          "location": {
              "type": "string",
              "description": "The city and state, e.g. San Francisco, CA"
          },
          "unit": {
              "type": "string",
              "enum": [
                  "celsius",
                  "fahrenheit",
              ]
          }
      },
      "required": [
          "location"
      ]
  },
)

weather_tool = generative_models.Tool(
  function_declarations=[get_current_weather_func]
)

prompt = "What is the weather like in Jakarta?"

model = GenerativeModel("gemini-pro", 
                        tools=[weather_tool])

chat = model.start_chat()

model_response = chat.send_message(prompt)

#print("model_response\n", model_response)
#print(model_response.candidates[0].content.parts[0].function_call.args['location'])

api_response = get_current_weather(model_response.candidates[0].content.parts[0].function_call.args['location'],"metric")

model_response = chat.send_message(
    Part.from_function_response(
        name="get_current_weather",
        response={
            "content": api_response,
        }
    ),
)

print("model_response\n", model_response)
print("model_response\n", model_response.text)